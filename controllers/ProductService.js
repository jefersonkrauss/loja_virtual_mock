'use strict';
var faker = require('faker')
var ip = require("ip");
var sleep = require('sleep')
var jsonfile = require('jsonfile')
var path = require('path');
var file = path.resolve(__dirname, '../db/product.json');
var generate = require('./generateProducts');

exports.getProductList = function (args, res, next) {
    
    //generate.generate(args, res, next);
    
    console.log("args: ", args);
    
    sleep.sleep(1);
    
    var q = "";
    var start = "";
    
    if (typeof(args.q) !== "undefined") 
        q = args.q.value;
    
    if (typeof(args.start) !== "undefined") 
        start = args.start.value;
    
    console.log("q: ", q);
    console.log("start: ", start);
    
    
    var busca = q.toLowerCase().split(" ");
    
    console.log("busca[]: ", busca);
    
    var products = [];
    var productsSearch = jsonfile.readFileSync(file);
    
    if ( q === null || q === "" ) {
        
        products = productsSearch;
        
    } else {
         
        console.log("Iniciando a busca por:", q);
        
        for ( var i=0; i<productsSearch.length; i++ ) {

            var obj = productsSearch[i];

            var fullTextSearch = obj.name + " " + obj.description + " " + obj.price;
            
            fullTextSearch = fullTextSearch.toLowerCase();
            
            console.log("fullTextSearch:", fullTextSearch);

            for (var j=0; j<busca.length; j++) {

                if ( fullTextSearch.indexOf(busca[j]) > -1 ) {
                    products.push(obj);
                }
            }

        }
        
    }
    
    var productsResult = [];
    
    var limit = 1;
    
    if ( start === null || start === "" || !parseInt(start) > 0 ) {
        
        productsResult = products;
        
    } else {
    
        for ( var i=0; i<products.length; i++ ) {

            if ( (i+1) >= parseInt(start) && limit <= 20) {

                productsResult.push(products[i]);

                limit++;

            }

            if (limit > 20) break;
        }
    }
    
    
    for (var p=0; p<productsResult.length; p++) {
        productsResult[p].imageUrl = productsResult[p].imageUrl.replace("IP_ADDRESS", ip.address());
        productsResult[p].imageThumbUrl = productsResult[p].imageThumbUrl.replace("IP_ADDRESS", ip.address());
    }
    
    var examples = {};
    examples['application/json'] = productsResult;
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
}
