/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var faker = require('faker')
var ip = require("ip");
var jsonfile = require('jsonfile')
var path = require('path');
var file = path.resolve(__dirname, '../db/product.json');

exports.generate = function (args, res, next) {
    
    var productsJson = [];
    var countSeqImage = 1;
    
    for (var i=0; i<2000; i++) {
        
        var hostImage = "http://IP_ADDRESS:3000";
        var image = "/images/f"+ countSeqImage + ".jpg";
        var imageThumb = "/thumb/f"+ countSeqImage + ".jpg";
        var amount = faker.finance.amount();
        var installments=[];
        
        var quantity;
        var count=0;
        
        var id = i+1000;
        
        for (count; count<10; count++) {
            quantity = count+1;
            installments.push({productId: id, quantity: quantity, valueByInstallment: (amount/quantity).toFixed(2)});
        }
        
        var product = {
            id:id,
            name: "Violão " + faker.name.jobDescriptor(),
            description: faker.name.title(),
            price: amount,
            currencySimbol: "R$",
            imageUrl: hostImage + image,
            imageThumbUrl: hostImage + imageThumb,
            installments: installments
        };
        
        productsJson.push(product);
        
        countSeqImage++;
        
        if (countSeqImage>21)countSeqImage=1;
        
    }

    jsonfile.writeFileSync(file, productsJson);
    
}
