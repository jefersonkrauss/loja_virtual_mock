'use strict';

var url = require('url');


var Payment = require('./PaymentService');


module.exports.getPayments = function getPayments (req, res, next) {
  Payment.getPayments(req.swagger.params, res, next);
};


module.exports.deletePaymentRequest = function deletePaymentRequest (req, res, next) {
  Payment.deletePaymentRequest(req.swagger.params, res, next);
};

module.exports.getPaymentRequestById = function getPaymentRequestById (req, res, next) {
  Payment.getPaymentRequestById(req.swagger.params, res, next);
};

module.exports.submitPaymentRequest = function submitPaymentRequest (req, res, next) {
  Payment.submitPaymentRequest(req.swagger.params, res, next);
};

module.exports.updatePaymentRequest = function updatePaymentRequest (req, res, next) {
  Payment.updatePaymentRequest(req.swagger.params, res, next);
};
