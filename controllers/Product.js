'use strict';

var url = require('url');


var Instrument = require('./ProductService');


module.exports.getProductList = function getProductList (req, res, next) {
  Instrument.getProductList(req.swagger.params, res, next);
};