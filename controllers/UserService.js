'use strict';
var sleep = require('sleep')
var jsonfile = require('jsonfile')
var path = require('path');
var fileUser = path.resolve(__dirname, '../db/user.json');


exports.createUser = function (args, res, next) {
    /**
     * parameters expected in the args:
     * body (User)
     **/
    // no response value expected for this operation
    console.log('CreateUser called')
    console.log("body: ", args.body)
    sleep.sleep(1);
    
    var UserCreated = args.body.value;
    
    var users = jsonfile.readFileSync(fileUser);
    console.log(users);
    console.log("Total de registros na tabela: ", users.length);
    var newId = users.length + 1;
    
    
    /**
     * Verificando se usuario existe
     */
    var userFounded=null;
    for(var i=0; i<users.length; i++){
        
        if (users[i].username === UserCreated.username){
            userFounded = users[i];
            console.log("Usuário encontrado: ", userFounded);
            break;
        }
        
    }
    
    /**
     * Registrando o novo usuario
     */
    if ( userFounded === null ) {
        
        console.log("cadastrando novo usuario: ", UserCreated)
        
        var User = {
            id: newId, 
            userStatus: 1,
            email: UserCreated.email,
            firstName: UserCreated.firstName,
            lastName: UserCreated.lastName,
            password: UserCreated.password,
            phone: UserCreated.phone,
            username: UserCreated.username
        }

        users.push(User);

        jsonfile.writeFileSync(fileUser, users)
        
    } else {
        
        res.statusCode = 400;
    }
    
    res.end();
}

exports.deleteUser = function (args, res, next) {
    /**
     * parameters expected in the args:
     * username (String)
     **/
    // no response value expected for this operation
    console.log("deleteUser called")
    console.log("body: ", args.body)
    sleep.sleep(1);
    
    
    var users = jsonfile.readFileSync(fileUser);
    console.log(users);
    console.log("Total de registros na tabela: ", users.length);
    
    /**
     * Verificando se usuario existe
     */
    var userFounded=null;
    for(var i=0; i<users.length; i++){
        
        if (users[i].username === args.username.value){
            
            userFounded = users[i];
            
            users.splice(i,1);
            
            jsonfile.writeFileSync(fileUser, users);
            
            break;
        }
        
    }
    
    if ( userFounded === null ) {
        res.statusCode = 400;
    }
    
    res.end();
}

exports.getUserByName = function (args, res, next) {

    console.log("getUserByName called")
    console.log("body: ", args.body)
    sleep.sleep(1);
    
    var users = jsonfile.readFileSync(fileUser);
    var user=null;
    for(var i=0; i<users.length; i++){
        
        if (users[i].username === args.username.value){
            user = users[i];
            console.log("Usuário encontrado: ", user);
            break;
        }
        
    }
    
    if ( user === null ) {
        res.statusCode = 400;
    }
    
    /**
     * parameters expected in the args:
     * username (String)
     **/
    var examples = {};
    examples['application/json'] = (user !== null) ? user : {};

    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.loginUser = function (args, res, next) {
    
    var status = 200;
    
    console.log("loginUser called")
    console.log("body: ", args.body)
    console.log("query: ", args.query)
    console.log("username: ", args.username.value)
    console.log("password: ", args.password.value)
    sleep.sleep(1);

    var users = jsonfile.readFileSync(fileUser);
    var user=null;
    for(var i=0; i<users.length; i++){
        
        if (users[i].username === args.username.value && 
                users[i].password === args.password.value) {
            user = users[i];
            console.log("Usuário encontrado: ", user);
            break;
        }
        
    }
    
    if ( user === null ) {
        console.log("usuario nao enconttrado")
        res.statusCode = 401;
    }

    /**
     * parameters expected in the args:
     * username (String)
     * password (String)
     **/
    var examples = {};
    
    examples['application/json'] = user === null ? "Usuário ou senha inválidos" : "Login eftuado com sucesso";
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.logoutUser = function (args, res, next) {

    console.log("logoutUser")
    console.log("body: ", args.body)
    sleep.sleep(1);

    /**
     * parameters expected in the args:
     **/
    // no response value expected for this operation
    res.end();
}

exports.updateUser = function (args, res, next) {

    console.log("updateUser")
    console.log("body: ", args.body)
    sleep.sleep(1);
    /**
     * parameters expected in the args:
     * username (String)
     * body (User)
     **/
    // no response value expected for this operation
    
    var users = jsonfile.readFileSync(fileUser);
    console.log(users);
    console.log("Total de registros na tabela: ", users.length);
    
    /**
     * Verificando se usuario existe
     */
    var UserUpdated = args.body.value;
    var userFounded=null;
    for(var i=0; i<users.length; i++){
        
        if (users[i].username === args.username.value){
            
            userFounded = users[i];
            
            userFounded.email = UserUpdated.email;
            userFounded.firstName = UserUpdated.firstName;
            userFounded.lastName = UserUpdated.lastName;
            userFounded.phone = UserUpdated.phone;
            
            users[i] = userFounded;
            
            break;
        }
        
    }
    
    if ( userFounded === null ) {
        res.statusCode = 400;
    } else {
        jsonfile.writeFileSync(fileUser, users);
    }
    
    res.end();
}

