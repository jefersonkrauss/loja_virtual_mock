'use strict';

var sleep = require('sleep')
var jsonfile = require('jsonfile')
var path = require('path');
var filePayment = path.resolve(__dirname, '../db/payment.json');

exports.getPayments = function (args, res, next) {
    
    /**
     * parameters expected in the args:
     * paymentId (Long)
     **/
    
    console.log('getPayments called');
    console.log("body: ", args.body);
    sleep.sleep(1);
    
    var payments = jsonfile.readFileSync(filePayment);
    console.log(payments);
    console.log("Total de registros na tabela: ", payments.length);
    
    
    var examples = {};
    
    examples['application/json'] = payments;
    
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.deletePaymentRequest = function (args, res, next) {
    /**
     * parameters expected in the args:
     * api_key (String)
     * paymentId (Long)
     **/
    // no response value expected for this operation
    console.log("deleteUser called")
    console.log("body: ", args.body)
    console.log("args: ", args)
    sleep.sleep(1);
    
    
    var payments = jsonfile.readFileSync(filePayment);
    console.log(payments);
    console.log("Total de registros na tabela: ", payments.length);
    
    /**
     * Verificando se pagamento existe
     */
    var found=null;
    for(var i=0; i<payments.length; i++){
        
        if (payments[i].id === args.paymentId.value){
            
            found = payments[i];
            
            payments.splice(i,1);
            
            jsonfile.writeFileSync(filePayment, payments);
            
            break;
        }
        
    }
    
    
    res.end();
}

exports.getPaymentRequestById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * paymentId (Long)
     **/
    
    console.log("deleteUser called")
    console.log("body: ", args.body)
    console.log("args: ", args)
    sleep.sleep(1);
    
    
    var payments = jsonfile.readFileSync(filePayment);
    console.log(payments);
    console.log("Total de registros na tabela: ", payments.length);
    
    /**
     * Verificando se pagamento existe
     */
    var found=null;
    for(var i=0; i<payments.length; i++){
        
        if (payments[i].id === args.paymentId.value){
            
            found = payments[i];
            
            break;
        }
        
    }
    
    
    var examples = {};
    
    examples['application/json'] = found;
    
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.submitPaymentRequest = function (args, res, next) {
    /**
     * parameters expected in the args:
     * body (PaymentRequest)
     **/
    // no response value expected for this operation
    console.log('submitPaymentRequest called')
    console.log("body: ", args.body)
    sleep.sleep(1);
    
    var paymentCreated = args.body.value;
    
    var payments = jsonfile.readFileSync(filePayment);
    console.log(payments);
    console.log("Total de registros na tabela: ", payments.length);
    var newId = payments.length + 1;
    
    var newPayment = {
        "reference": paymentCreated.reference,
        "date": paymentCreated.date,
        "sender": {
            "name": paymentCreated.sender.name,
            "email": paymentCreated.sender.email
        },
        "name": paymentCreated.name,
        "description": paymentCreated.description,
        "id": newId
    };
    
    payments.push(newPayment);

    jsonfile.writeFileSync(filePayment, payments);
    
    res.end();
}

exports.updatePaymentRequest = function (args, res, next) {
    /**
     * parameters expected in the args:
     * body (PaymentRequest)
     **/
    // no response value expected for this operation
    res.end();
}

