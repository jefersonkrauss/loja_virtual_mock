---
swagger: "2.0"
info:
  description: "This a sample API to be used in M4U Android Recruitment App\n"
  version: "1.0.0"
  title: "M4U Android Recruitment App"
  termsOfService: "http://www.m4u.com.br/terms/"
  contact:
    name: "rh@m4u.com.br"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "recruitment.m4u.com.br"
basePath: "/v1"
schemes:
- "http"
paths:
  /payment:
    get:
      tags:
      - "payment"
      summary: "List of payments"
      description: "Returns a list of payments"
      operationId: "getPayments"
      produces:
      - "application/json"
      - "application/xml"
      parameters: []
      responses:
        default:
          description: "successful operation"
          schema:
            $ref: "#/definitions/PaymentRequest"
      x-swagger-router-controller: "Payment"
    post:
      tags:
      - "payment"
      summary: "Submits a new Payment Request"
      description: ""
      operationId: "submitPaymentRequest"
      consumes:
      - "application/json"
      - "application/xml"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - in: "body"
        name: "body"
        description: "Payment Request"
        required: false
        schema:
          $ref: "#/definitions/PaymentRequest"
      responses:
        405:
          description: "Invalid input"
      x-swagger-router-controller: "Payment"
    put:
      tags:
      - "payment"
      summary: "Update an existing Payment Request"
      description: ""
      operationId: "updatePaymentRequest"
      consumes:
      - "application/json"
      - "application/xml"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - in: "body"
        name: "body"
        description: "Payment Request object that needs to be added to the store"
        required: true
        schema:
          $ref: "#/definitions/PaymentRequest"
      responses:
        400:
          description: "Invalid ID supplied"
        404:
          description: "Payment Request not found"
        405:
          description: "Validation exception"
      x-swagger-router-controller: "Payment"
    
  /payment/{paymentId}:
    get:
      tags:
      - "payment"
      summary: "Find Payment Request by ID"
      description: "Returns a Payment Request when ID < 10.  ID > 10 or nonintegers\
        \ will simulate API error conditions"
      operationId: "getPaymentRequestById"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "paymentId"
        in: "path"
        description: "ID of PaymentRequest that needs to be fetched"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/PaymentRequest"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Payment Request not found"
      x-swagger-router-controller: "Payment"
    delete:
      tags:
      - "payment"
      summary: "Deletes a Payment Request"
      description: ""
      operationId: "deletePaymentRequest"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "api_key"
        in: "header"
        description: ""
        required: true
        type: "string"
      - name: "paymentId"
        in: "path"
        description: "Payment Request id to delete"
        required: true
        type: "integer"
        format: "int64"
      responses:
        400:
          description: "Invalid payment request value"
      x-swagger-router-controller: "Payment"
  /users:
    post:
      tags:
      - "user"
      summary: "Create user"
      description: "This can only be done by the logged in user."
      operationId: "createUser"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - in: "body"
        name: "body"
        description: "Created user object"
        required: false
        schema:
          $ref: "#/definitions/User"
      responses:
        default:
          description: "successful operation"
      x-swagger-router-controller: "User"
  /users/login:
    get:
      tags:
      - "user"
      summary: "Logs user into the system"
      description: ""
      operationId: "loginUser"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "username"
        in: "query"
        description: "The user name for login"
        required: false
        type: "string"
      - name: "password"
        in: "query"
        description: "The password for login in clear text"
        required: false
        type: "string"
      responses:
        200:
          description: "successful operation"
          schema:
            type: "string"
        400:
          description: "Invalid username/password supplied"
      x-swagger-router-controller: "User"
  /users/logout:
    get:
      tags:
      - "user"
      summary: "Logs out current logged in user session"
      description: ""
      operationId: "logoutUser"
      produces:
      - "application/json"
      - "application/xml"
      parameters: []
      responses:
        default:
          description: "successful operation"
      x-swagger-router-controller: "User"
  /users/{username}:
    get:
      tags:
      - "user"
      summary: "Get user by user name"
      description: ""
      operationId: "getUserByName"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "username"
        in: "path"
        description: "The name that needs to be fetched. Use user1 for testing."
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/User"
        400:
          description: "Invalid username supplied"
        404:
          description: "User not found"
      x-swagger-router-controller: "User"
    put:
      tags:
      - "user"
      summary: "Updated user"
      description: "This can only be done by the logged in user."
      operationId: "updateUser"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "username"
        in: "path"
        description: "name that need to be deleted"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Updated user object"
        required: false
        schema:
          $ref: "#/definitions/User"
      responses:
        400:
          description: "Invalid user supplied"
        404:
          description: "User not found"
      x-swagger-router-controller: "User"
    delete:
      tags:
      - "user"
      summary: "Delete user"
      description: "This can only be done by the logged in user."
      operationId: "deleteUser"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
      - name: "username"
        in: "path"
        description: "The name that needs to be deleted"
        required: true
        type: "string"
      responses:
        400:
          description: "Invalid username supplied"
        404:
          description: "User not found"
      x-swagger-router-controller: "User"
  /products:
    get:
      tags:
      - "product"
      summary: "List of products"
      description: "Returns a list of products"
      operationId: "getProductList"
      produces:
      - "application/json"
      - "application/xml"
      parameters:
        - name: "q"
          in: "query"
          description: ""
          required: false
          type: "string"
        - name: "start"
          in: "query"
          description: ""
          required: false
          type: "string"
      responses:
        default:
          description: "successful operation"
          schema:
            $ref: "#/definitions/Product"
      x-swagger-router-controller: "Product"
    
  
definitions:
  User:
    properties:
      id:
        type: "integer"
        format: "int64"
      username:
        type: "string"
      firstName:
        type: "string"
      lastName:
        type: "string"
      email:
        type: "string"
      password:
        type: "string"
      phone:
        type: "string"
      userStatus:
        type: "integer"
        format: "int32"
        description: "User Status"
  Sender:
    properties:
      name:
        type: "string"
      email:
        type: "string"
  PaymentRequest:
    required:
    - "date"
    - "description"
    - "name"
    - "reference"
    - "sender"
    properties:
      id:
        type: "integer"
        format: "int64"
      sender:
        $ref: "#/definitions/Sender"
      name:
        type: "string"
      description:
        type: "string"
      reference:
        type: "string"
      date:
        type: "string"
        format: "date-time"
  Installment:
    properties:
      productId:
        type: "integer"
        format: "int64"
      quantity:
        type: "integer"
        format: "int32"
      valueByInstallment:
        type: "number"
  Product:
    properties:
      id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
      description:
        type: "string"
      price:
        type: "number"
      currencySimbol:
        type: "string"
      imageUrl:
        type: "string"
      imageThumbUrl:
        type: "string"
      installments:
        type: "array"
        items:
            $ref: "#/definitions/Installment"