# Api Rest Mock - Loja Virtual #

### Instalação ###


Para rodar esta aplicação se faz necessário instalar a versão mais atual do node e do npm.


Na raiz do projeto, temos um arquivo chamado **package.json**, contendo todas as dependências do projeto. Para realizar a instalação, é necessário executar o comando **npm install** na raiz do projeto, onde se encontra package file.

### Exemplo ###

```bash
cd project_folder
npm install
```

### Inicializando os serviços ###

Na raiz do projeto, executar o comando abaixo:

```bash
node index.js
```

### Considerações ###

Os serviços que serão inicializados irão utilizar as portas **8080** e **3000**